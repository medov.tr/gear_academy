#![no_std]
use core::panic;

use gstd::{exec, msg, prelude::*, ActorId};
use auction_io::*;
use nft_io::*;
use ft_io::*;

#[derive(Debug, Default)]
pub struct Auction {
    pub auction_id: ActorId,
    pub seller: ActorId,
    pub nft_contract_id: ActorId,
    pub ft_cotract_id: ActorId,
    pub token_id: TokenId,
    pub current_bid: u128,
    pub current_bidder: ActorId,
    pub ended_at: u64,
    pub auction_is_on: bool,
}

static mut AUCTION: Option<Auction> = None;
const BID_PERIOD: u64 = 5 * 60 * 1000;
const ZERO_ID: ActorId = ActorId::new([0u8; 32]);

impl Auction {
    async fn start_auction(
        &mut self,
        auction_id: &ActorId,
        nft_contract_id: &ActorId,
        ft_contract_id: &ActorId,
        token_id: TokenId,
        duration: u64,
        min_price: u128,
    ) {

        let check_owner = msg::send_for_reply_as::<_, NFTEvent>(
                                        *nft_contract_id,
                                        NFTAction::GetOwner { token_id: token_id },
                                        0,
                                    )
                                    .expect("Error in sending async message [NFTAction::GetOwner]")
                                    .await
                                    .expect("Error in async message [NFTAction::GetOwner]");
    
        match check_owner {
            NFTEvent::GetOwner { owner } => 
            {
                if owner != msg::source(){
                    panic!("Only the owner can start the auction");
                }
            },
            _ => (),
        }

        if self.auction_is_on {
            panic!("Auction already exists")
        }

        if duration < BID_PERIOD {
            panic!("Auction duration must be equal or more than bid period")
        }
        
        if min_price <= 500 {
            panic!("Price must be more than 500")
        }

        self.nft_contract_id = *nft_contract_id;
        self.ft_cotract_id = *ft_contract_id;
        self.auction_id = *auction_id;
        self.token_id = token_id;
        self.ended_at = exec::block_timestamp() + duration;
        self.current_bid = min_price;
        self.auction_is_on = true;
        self.seller = msg::source();

        msg::reply(AuctionEvent::AuctionStarted {
                auction_id: *auction_id,
                nft_contract_id: *nft_contract_id,
                ft_contract_id: *ft_contract_id,
                token_id,
                min_price,
                //ended_at: self.ended_at
            },
            0,
        ).expect("Error in reply [AuctionEvent::AuctionStarted]");

    }


    async fn make_bid(&mut self, bid: u128) {

        if !self.auction_is_on {
            panic!("Auction must exists")
        }

        if exec::block_timestamp() > self.ended_at {
            panic!("Auction is over")
        }
        if bid <= self.current_bid {
            panic!("Your bid must be more than the current bid")
        }

        if self.current_bidder == ZERO_ID {

            msg::send_for_reply_as::<_, FTEvent>(
                self.ft_cotract_id,
                FTAction::Transfer { from: msg::source(), to: self.auction_id, amount: bid.into() },
                0,
            )
            .expect("Error in sending async message [FTAction::Transfer]")
            .await
            .expect("Error in async message [FTAction::Transfer]");


        }
        else {
            msg::send_for_reply_as::<_, FTEvent>(
                self.ft_cotract_id,
                FTAction::Transfer { from: self.auction_id, to: self.current_bidder, amount: self.current_bid.into() },
                0,
            )
            .expect("Error in sending async message [FTAction::Transfer]")
            .await
            .expect("Error in async message [FTAction::Transfer]");

            msg::send_for_reply_as::<_, FTEvent>(
                self.ft_cotract_id,
                FTAction::Transfer { from: msg::source(), to: self.auction_id, amount: bid.into() },
                0,
            )
            .expect("Error in sending async message [FTAction::Transfer]")
            .await
            .expect("Error in async message [FTAction::Transfer]");

        }
        
        self.current_bid = bid;
        self.current_bidder = msg::source();

        if exec::block_timestamp() + BID_PERIOD > self.ended_at {
            self.ended_at = exec::block_timestamp() + BID_PERIOD;
        }

        

        msg::reply(
            AuctionEvent::BidMade {
                bid: bid,
             },
             0,
        ).expect("Error in reply [AuctionEvent::BidMade]");
    }


    async fn end_auction(&mut self) {


        if !self.auction_is_on {
            panic!("Auction must exists")
        }

        if exec::block_timestamp() <= self.ended_at {
            panic!("Auction must be over")
        }

        if self.seller != msg::source(){
            panic!("Only the owner can finish the auction");
        }


        if self.current_bidder != ZERO_ID {
            msg::send_for_reply_as::<_, NFTEvent>(
                self.nft_contract_id,
                NFTAction::Transfer {
                    to: self.current_bidder,
                    token_id: self.token_id,
                },
                0,
            )
            .expect("Error in sending async message [NFTAction::Transfer]")
            .await
            .expect("Error in async message [NFTAction::Transfer]");
                    
            
            msg::send_for_reply_as::<_, FTEvent>(
                self.ft_cotract_id,
                FTAction::Transfer { from: self.auction_id, to: self.seller, amount: self.current_bid.into() },
                0,
            )
            .expect("Error in sending async message [FTAction::Transfer]")
            .await
            .expect("Error in async message [FTAction::Transfer]");
            
        }

        self.auction_is_on = false;
        msg::reply(
            AuctionEvent::AuctionEnded {
                winner: self.current_bidder,
                highest_bid: self.current_bid,
                },
                0,
            )
            .expect("Error in reply [AuctionEvent::AuctionEnded]");
            
        self.current_bidder = ZERO_ID;
        
    }
}

#[gstd::async_main]

async unsafe fn main() {
    let action: AuctionAction = msg::load().expect("Could not load Action");
    let auction: &mut Auction = unsafe { AUCTION.get_or_insert(Auction::default()) };

    match action {
        AuctionAction::StartAuction {
            auction_id,
            nft_contract_id,
            ft_contract_id,
            token_id,
            duration,
            min_price,
        } => auction.start_auction(&auction_id, &nft_contract_id,&ft_contract_id, token_id, duration, min_price).await,

        AuctionAction::MakeBid {bid} => auction.make_bid(bid).await,

        AuctionAction::EndAuction => auction.end_auction().await,
    }
}

