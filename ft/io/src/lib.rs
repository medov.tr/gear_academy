#![no_std]
use codec::{Decode, Encode};
use gstd::{prelude::*, ActorId};
use primitive_types::U256;
use scale_info::TypeInfo;
pub type Amount = U256;

#[derive(Debug, Default, Encode, Decode, TypeInfo)]
pub struct InitFT {
    pub name: String,
    pub symbol: String,
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum FTAction {
    Mint {
        to: ActorId,
        amount: Amount,
    },
    Burn {
        amount: Amount,
    },
    Transfer {
        from: ActorId,
        to: ActorId,
        amount: Amount,
    },
    Approve {
        to: ActorId,
        amount: Amount,
    },
    RevokeApproval {
        to: ActorId,
        amount: Amount,
    },
}


#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum FTEvent {
    Transfer {
        from: ActorId,
        to: ActorId,
        amount: Amount,
    },
    Approval {
        from: ActorId,
        approved_account: ActorId,
        amount: Amount,
    },
    Cancelapproval {
        from: ActorId,
        denial_account: ActorId,
        amount: Amount,
    },
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum FTMetaState {
    FTInfo,
    Balance{owner: ActorId},
    Approvals{owner: ActorId},
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum FTMetaStateReply {
    FTInfo {
        name: String,
        symbol: String,
    },
    Balance {
        amount: Amount,
    },
    Approvals {
        approvals: BTreeMap<ActorId, Amount>,
    },
}