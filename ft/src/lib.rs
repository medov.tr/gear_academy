#![no_std]
use gstd::{msg, prelude::*, ActorId};
use ft_io::*;
const ZERO_ID: ActorId = ActorId::new([0u8; 32]);

#[derive(Debug, Default, Clone)]
pub struct FungibleToken {
    pub name: String,
    pub symbol: String,
    pub account_by_id: BTreeMap<ActorId, Amount>,
    pub approvals: BTreeMap<ActorId, BTreeMap<ActorId, Amount>>,
}

static mut FUNGIBLE_TOKEN: Option<FungibleToken> = None;

impl FungibleToken {

    fn assert_can_transfer(&mut self, from: &ActorId, amount: Amount) -> bool{
        let approved_account = &msg::source();
        if from == approved_account {
            return true;
        }

        if let Some(approved_accounts) = self.approvals.get(from) {
            if approved_accounts.contains_key(&msg::source()) {
                if let Some(allowed_amount) = approved_accounts.get(approved_account) {
                    if allowed_amount >= &amount {
                        self.approvals
                        .entry(*from)
                        .and_modify(|map_approvals| 
                            {map_approvals.entry(*approved_account).and_modify(|number| {*number -= amount});});
                        return true;
                    }
                }
            }
        }
        false 
    }


    fn assert_zero_address(&self, account: &ActorId) {
        if account == &ZERO_ID {
            panic!("FungibleToken: Zero address");
        }
    }


    fn assert_balance(&self, from: &ActorId, amount: Amount) {
        if self.account_by_id[from] < amount {
            panic!("FungibleToken: insufficient funds");
        }
    }

    fn mint(&mut self, to: &ActorId, amount: Amount) {
        
        self.assert_zero_address(to); // проверка на нулевой адресс
        self.account_by_id // пополняем BTreeMap<ActorId, Vec<TokenId>>
            .entry(*to)     // если еще нету такого такого пользователя, то создаем
            .and_modify(|amounts| {*amounts += amount}) // если есть, то просто пополянем баланс
            .or_insert(amount);
        
        msg::reply(
            FTEvent::Transfer {
                from: ZERO_ID,
                to: *to,
                amount,
            },
            0,
        )
        .expect("Error in reply [FTEvent::Transfer] in mint function");
    }

    fn transfer(&mut self, from: &ActorId,  to: &ActorId, amount: Amount) {
        self.assert_zero_address(to);
        if !self.assert_can_transfer(from, amount){
            panic!("Not allowed to transfer")
        }
        self.assert_balance(from, amount);

        self.account_by_id
        .entry(*to)
        .and_modify(|amounts| {*amounts += amount})
        .or_insert(amount);

        self.account_by_id 
        .entry(*from)
        .and_modify(|amounts| {*amounts -= amount})
        .or_insert(amount);

        msg::reply(
            FTEvent::Transfer {
                from: *from,
                to: *to,
                amount,
            },
            0,
        )
        .expect("Error in reply [FTEvent::Transfer] in transfer function");
        
    }


    fn approve(&mut self, to: &ActorId, amount: Amount) {
        
        let owner = &msg::source();

        self.assert_zero_address(to);
        self.assert_balance(owner, amount);
        self.approvals
            .entry(*owner)
            .and_modify(|map_approvals| 
                {map_approvals.entry(*to).and_modify(|number| {*number += amount});})
            .or_insert(BTreeMap::from([(*to,amount)]));

        msg::reply(
            FTEvent::Approval {
                from: *owner,
                approved_account: *to,
                amount,
            },
            0,
        )
        .expect("Error in reply [FTEvent::Approval]");
    }


    fn burn(&mut self, amount: Amount) {

        let owner = &msg::source();
        if self.account_by_id[owner] < amount {
            panic!("Burn: You cannot delete more than {} {}", self.account_by_id[owner], self.name);
        }
        else {
            self.account_by_id 
                .entry(*owner)
                .and_modify(|amounts| {*amounts -= amount})
                .or_insert(amount);
        }

        msg::reply(
            FTEvent::Transfer {
                from: *owner,
                to: ZERO_ID,
                amount,
            },
            0,
        )
        .expect("Error in reply [FTEvent::Transfer] in burn function");
    
    }
    
    fn revoke_approval(&mut self, denial_account: &ActorId, amount: Amount){

        let owner = &msg::source();

        if self.approvals[owner][denial_account] < amount {
            self.approvals
                .entry(*owner)
                .and_modify(|map_approvals| 
                    {map_approvals.remove(denial_account);});
        }
        else {
            self.approvals
                .entry(*owner)
                .and_modify(|map_approvals| 
                    {map_approvals.entry(*denial_account).and_modify(|number| {*number -= amount});});
        }

        msg::reply(
            FTEvent::Cancelapproval { 
                from: *owner, 
                denial_account: *denial_account, 
                amount},
            0,
        )
        .expect("Error in reply [FTEvent::Сancel_approval] in revoke_approval function");
    }

}

#[no_mangle]
pub unsafe extern "C" fn handle() {
    let action: FTAction = msg::load().expect("Could not load Action");
    let ft: &mut FungibleToken = FUNGIBLE_TOKEN.get_or_insert(FungibleToken::default());
    match action {
        FTAction::Mint { to, amount } => ft.mint(&to, amount),

        FTAction::Burn { amount } => ft.burn(amount),

        FTAction::RevokeApproval { to, amount } => ft.revoke_approval(&to, amount),

        FTAction::Transfer { from, to, amount } => ft.transfer(&from, &to, amount),

        FTAction::Approve { to, amount } => ft.approve(&to, amount),
    }
}




#[no_mangle]
pub unsafe extern "C" fn init() {
    let config: InitFT = msg::load().expect("Unable to decode init message");
    let ft = FungibleToken {
        name: config.name,
        symbol: config.symbol,
        ..FungibleToken::default()
    };
    FUNGIBLE_TOKEN = Some(ft);
}


#[no_mangle]
pub unsafe extern "C" fn meta_state() -> *mut [i32; 2] {

    let state = msg::load().expect("failed to decode FTMetaState");
    let ft: &mut FungibleToken = FUNGIBLE_TOKEN.get_or_insert(FungibleToken::default());

    let encoded = match state {
        FTMetaState::FTInfo => FTMetaStateReply::FTInfo{
            name: ft.name.clone(),
            symbol: ft.symbol.clone(),
        }.encode(),

        FTMetaState::Balance { owner } => FTMetaStateReply::Balance {
            amount: ft.account_by_id[&owner].clone(),
        }.encode(),

        FTMetaState::Approvals { owner } => FTMetaStateReply::Approvals {
            approvals: ft.approvals[&owner].clone(),
        }.encode(),
    };
    gstd::util::to_leak_ptr(encoded)
}



// #[cfg(test)]
// extern crate std;
// #[cfg(test)]
// use std::println;

#[cfg(test)]
mod tests {
    use crate::*;
    use gtest::{Program, RunResult, System};

    pub const USER_1: u64 = 2;
    pub const AMOUNT_1: u128 = 500;
    pub const AMOUNT_2: u128 = 250;
    pub const USER_2: u64 = 3;
    pub const USER_3: u64 = 4;

    fn init_ft(sys: &System) {
        sys.init_logger();
        let ft = Program::current(&sys);
        let res = ft.send(
            USER_1,
            InitFT {
                name: String::from("Timurium"),
                symbol: String::from("Eagle"),
            },
        );
        assert!(res.log().is_empty());
    }

    fn mint(ft: &Program, from: u64, to: ActorId, amount: Amount,) -> RunResult {
        ft.send(from, FTAction::Mint {to: to, amount: amount})
    }

    fn transfer(ft: &Program, from: u64, to: ActorId, amount: Amount) -> RunResult {
        ft.send(from, FTAction::Transfer {from: from.into(),  to: to, amount: amount })
    }

    fn approve(ft: &Program, from: u64, to: ActorId, amount: Amount) -> RunResult {
        ft.send(from, FTAction::Approve  { to: to, amount: amount })
    }
    fn burn(ft: &Program, from: u64, amount: Amount) -> RunResult {
        ft.send(from, FTAction::Burn { amount: amount })
    }
    fn revoke_approval(ft: &Program, from: u64, to: ActorId, amount: Amount) -> RunResult {
        ft.send(from, FTAction::RevokeApproval { to: to, amount: amount } )
    }

    #[test]
    fn mint_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        let res = mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        assert!(res.contains(&(
            USER_1,
            FTEvent::Transfer {
                from: ZERO_ID,
                to: USER_1.into(),
                amount: AMOUNT_1.into(),
            }.encode()
        )));

        let state: FTMetaStateReply = ft.meta_state(FTMetaState::FTInfo).expect("Error in meta_state: FTInfo ");
        match state{
            FTMetaStateReply::FTInfo { name, symbol } => assert_eq!((name,symbol),(String::from("Timurium"), String::from("Eagle"))),
            _ => (),
        }

        let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USER_1.into() } ).expect("Error in meta_state: Balance");
        // println!("Balance: {:?}", balance);
        match balance{
            FTMetaStateReply::Balance { amount } => assert_eq!(amount, AMOUNT_1.into()),
            _ => (),
        }
    }

    #[test]
    fn transfer_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        let res = transfer(&ft, USER_1, USER_2.into(), AMOUNT_1.into());
        assert!(res.contains(&(
            USER_1,
            FTEvent::Transfer { 
                from: USER_1.into(),
                to: USER_2.into(),
                amount: AMOUNT_1.into() 
            }.encode()
        )));

        // let balance_1: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USER_1.into() } ).expect("Error in meta_state: Balance");
        let balance_2: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USER_2.into() } ).expect("Error in meta_state: Balance");
        // println!("Balance_1: {:?}", balance_1);
        // println!("Balance_2: {:?}", balance_2);

        match balance_2{
            FTMetaStateReply::Balance { amount } => assert_eq!(amount, AMOUNT_1.into()),
            _ => (),
        }
        // assert!(transfer(&ft, USER_1, USER_2.into(), AMOUNT_1.into()).main_failed());
    }

    #[test]
    fn approve_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        let res = approve(&ft, USER_1, USER_2.into(), AMOUNT_1.into());
        assert!(res.contains(&(
            USER_1,
            FTEvent::Approval {
                from: USER_1.into(),
                approved_account: USER_2.into(),
                amount: AMOUNT_1.into(),
            }.encode()
        )));

        let approval: FTMetaStateReply = ft.meta_state(FTMetaState::Approvals { owner: USER_1.into() }).expect("Error in meta_state: Approval");

        match approval{
            FTMetaStateReply::Approvals { approvals } => assert_eq!(approvals[&USER_2.into()], AMOUNT_1.into()),
            _ => (),
        }
    }

    #[test]
    fn burn_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        let res = burn(&ft, USER_1, AMOUNT_2.into());
        assert!(res.contains(&(
            USER_1,
            FTEvent::Transfer {
                from: USER_1.into(),
                to: ZERO_ID,
                amount: AMOUNT_2.into(),
            }.encode()
        )));

        let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USER_1.into() }).expect("Error in meta_state: Balance");
        match balance{
            FTMetaStateReply::Balance { amount } => assert_eq!(amount, AMOUNT_2.into()),
            _ => (),
        }
    }


    #[test]
    fn revoke_approval_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        approve(&ft, USER_1, USER_2.into(), AMOUNT_1.into());
        let res = revoke_approval(&ft, USER_1, USER_2.into(), AMOUNT_2.into());
        //println!("{:?}", res.log());
        assert!(res.contains(&(
            USER_1,
            FTEvent::Cancelapproval {
                from: USER_1.into(),
                denial_account: USER_2.into(),
                amount: AMOUNT_2.into(),
            }.encode()
        )));

        let approval: FTMetaStateReply = ft.meta_state(FTMetaState::Approvals { owner: USER_1.into() }).expect("Error in meta_state: Approval");
        match approval{
            FTMetaStateReply::Approvals { approvals } => assert_eq!(approvals[&USER_2.into()], AMOUNT_2.into()),
            _ => (),
        }
    }

    #[test]
    fn approve_and_transfer_success() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        mint(&ft, USER_1, USER_1.into(), AMOUNT_1.into());
        approve(&ft, USER_1, USER_2.into(), AMOUNT_1.into());
        let res = ft.send(USER_2, FTAction::Transfer { from: USER_1.into(), to: USER_3.into(), amount: (AMOUNT_1 - 100).into() });

        assert!(res.contains(&(
            USER_2,
            FTEvent::Transfer { 
                from: USER_1.into(),
                to: USER_3.into(),
                amount: (AMOUNT_1 - 100).into() 
            }.encode()
        )));
        let approval: FTMetaStateReply = ft.meta_state(FTMetaState::Approvals { owner: USER_1.into() }).expect("Error in meta_state: Approval");

        match approval{
            FTMetaStateReply::Approvals { approvals } => assert_eq!(approvals[&USER_2.into()], (AMOUNT_1-400).into()),
            _ => (),
        }
    }

    #[test]
    fn mint_failures() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        // should fall because the user tried to skip to a null address
        assert!(mint(&ft, USER_1, ZERO_ID, AMOUNT_1.into()).main_failed());
        
    }

    #[test]
    fn transfer_failures() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);

        mint(&ft, USER_1, USER_1.into(), AMOUNT_2.into());
        // should fall because the user tried to skip to a null address
        assert!(transfer(&ft, USER_1, ZERO_ID, AMOUNT_2.into()).main_failed());
        // should fall because insufficient funds
        assert!(transfer(&ft, USER_1, USER_2.into(), AMOUNT_1.into()).main_failed());   
    }
    #[test]
    fn approve_failures() {
        let sys = System::new();
        init_ft(&sys);
        let ft = sys.get_program(1);
        
        mint(&ft, USER_1, USER_1.into(), AMOUNT_2.into());
        // should fall because the user tried to skip to a null address
        assert!(approve(&ft, USER_1, ZERO_ID, AMOUNT_2.into()).main_failed());
        // should fall because insufficient funds
        assert!(approve(&ft, USER_1, USER_2.into(), AMOUNT_1.into()).main_failed());   
    }
    
}