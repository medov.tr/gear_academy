use codec::{Decode, Encode};
use gtest::{Program, System, WasmProgram};
use nft_io::*;
use ft_io::*;

pub const USERS: &'static [u64] = &[5, 6, 7, 8];
pub const SELLER: u64 = 10;
pub const AUCTION_ID: u64 = 2;
pub const DURATION: u64 = 2 * 24 * 60 * 60 * 1000;
pub const MIN_PRICE: u64 = 1000;

pub fn init_ft(sys: &System) {
    sys.init_logger();
    let ft = Program::from_file(
        &sys,
        "../ft/target/wasm32-unknown-unknown/release/fungible_token.wasm",
    );

    assert!(ft
        .send(
            USERS[0],
            InitFT {
                name: String::from("MyToken"),
                symbol: String::from("MTK"),
            },
        )
        .log()
        .is_empty());
}


#[derive(Debug)]
pub struct NonFungibleToken;

impl WasmProgram for NonFungibleToken {
    fn init(&mut self, _: Vec<u8>) -> Result<Option<Vec<u8>>, &'static str> {

        Ok(Some(b"GOT IT".to_vec()))
    }

    fn handle(&mut self, payload: Vec<u8>) -> Result<Option<Vec<u8>>, &'static str> {
        let res = NFTAction::decode(&mut &payload[..]).map_err(|_| "Can't decode!")?;
        match res {
            NFTAction::Transfer { to: _, token_id: _ } => {
                return Ok(Some(
                    NFTEvent::Transfer {
                        from: 0.into(),
                        to: 0.into(),
                        token_id: 0.into(),
                    }
                    .encode(),
                ));
            },
            NFTAction::GetOwner { token_id: _ } => {
                return Ok(Some(
                    NFTEvent::GetOwner {
                        owner: SELLER.into(),
                        
                    }
                    .encode(),
                ));
            },
            _ => return Ok(None),
        }
    }

    fn handle_reply(&mut self, _: Vec<u8>) -> Result<Option<Vec<u8>>, &'static str> {
        Ok(None)
    }
    fn meta_state(&mut self, _payload: Option<Vec<u8>>) -> Result<Vec<u8>, &'static str> {
        Ok(Vec::new())
    }
}

pub fn init_mock_nft(sys: &System) {
    let nft = Program::mock(&sys, NonFungibleToken);
    let res = nft.send_bytes(SELLER, "INIT");
    assert!(!res.log().is_empty());
}