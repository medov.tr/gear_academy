#![no_std]

use codec::{Decode, Encode};
use gstd::{prelude::*, ActorId};
use primitive_types::U256;
use scale_info::TypeInfo;
pub type TokenId = U256;

#[derive(Debug, Default, Encode, Decode, TypeInfo, Clone)]
pub struct TokenMetadata {
    // ex. "CryptoKitty #100"
    pub title:String,
    // free-form description
    pub description: String,
    // URL to associated media, preferably to decentralized, content-addressed storage
    pub media: String,
    // URL to an off-chain JSON file with more info.
    pub reference: String, // URL to an off-chain JSON file with more info.
}

#[derive(Debug, Default, Encode, Decode, TypeInfo)]
pub struct InitNFT {
    pub name: String,
    pub symbol: String,
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum NFTAction {
    Mint {
        to: ActorId,
        token_id: TokenId,
        token_metadata: Option<TokenMetadata>,
    },
    Burn {
        token_id: TokenId,
    },
    Transfer {
        to: ActorId,
        token_id: TokenId,
    },
    Approve {
        to: ActorId,
        token_id: TokenId,
    },
    RevokeApproval {
        to: ActorId,
        token_id: TokenId,
    },
    GetOwner {
        token_id: TokenId,
    },
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum NFTEvent {
    Transfer {
        from: ActorId,
        to: ActorId,
        token_id: TokenId,
    },
    Approval {
        owner: ActorId,
        approved_account: ActorId,
        token_id: TokenId,
    },
    Cancelapproval {
        owner: ActorId,
        denial_account: ActorId,
        token_id: TokenId,
    },
    GetOwner {
        owner: ActorId
    },
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum NFTMetaState {
    NFTInfo,
    TokensForOwner{token: TokenId},
    TotalSupply,
    SupplyForOwner{owner: ActorId},
    AllTokens,
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum NFTMetaStateReply {
    NFTInfo {
        name: String,
        symbol: String,
    },
    TokensForOwner {
        token_approvals: Vec<ActorId>,
        token_metadata_by_id: Option<TokenMetadata>,

    },
    TotalSupply(u128),
    SupplyForOwner(u128),
    AllTokens {
        owner_by_id: BTreeMap<TokenId, ActorId>,
        token_approvals: BTreeMap<TokenId, Vec<ActorId>>,
        token_metadata_by_id: BTreeMap<TokenId, Option<TokenMetadata>>,

    },
}