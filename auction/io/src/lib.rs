#![no_std]

use codec::{Decode, Encode};
use gstd::{prelude::*, ActorId};
use scale_info::TypeInfo;
use nft_io::TokenId;

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum AuctionAction {
    StartAuction {
        auction_id: ActorId,
        nft_contract_id: ActorId,
        ft_contract_id: ActorId,
        token_id: TokenId,
        duration: u64,
        min_price: u128,
    },
    MakeBid {
        bid: u128,
    },
    EndAuction,
}

#[derive(Debug, Encode, Decode, TypeInfo)]
pub enum AuctionEvent {
    AuctionStarted {
        auction_id: ActorId,
        nft_contract_id: ActorId,
        ft_contract_id: ActorId,
        token_id: TokenId,
        min_price: u128,
        //ended_at: u64,
    },
    BidMade {
        bid: u128,
    },
    AuctionEnded {
        winner: ActorId,
        highest_bid: u128,
    },
}
