#![no_std]
#![allow(non_snake_case)]

use gstd::{msg, debug, prelude::*};
static mut GREETING: String = String::new();

#[no_mangle]
pub unsafe extern "C" fn handle() {
    let new_msg = String::from_utf8(msg::load_bytes()).expect("Invalid message");
    if new_msg == "PING" {
        msg::reply(b"PONG", 0).expect("Error in sending reply");
    }
    else {
        panic!("Panic: message isn't PING");
    }
    
}

#[no_mangle]
pub unsafe extern "C" fn init() {
    GREETING = String::from_utf8(msg::load_bytes()).expect("Invalid message");
    debug!("Program was initialized with '{}' greeting", GREETING);
}

#[cfg(test)]
 mod tests {

    use gtest::{Program, System};
    #[test]

    fn test_ping() {
        let system = System::new();
        system.init_logger();
        let program = Program::current(&system);
        
        // Initial
        let mut res = program.send_bytes(2, "");
        assert!(res.log().is_empty());
        // Handle
        let ping = "PING";
        let pong = "PONG";
        res = program.send_bytes(2, ping);
        assert!(res.contains(&(2, pong)));
    }

    #[test]
    fn test_ping_2() {
        let system = System::new();
        system.init_logger();
        let program = Program::current(&system);
        
        // Initial
        let mut res = program.send_bytes(2, "");
        assert!(res.log().is_empty());
        // Handle
        let pong = "PONG";
        res = program.send_bytes(2, pong);
        assert!(res.contains(&(2, pong)) == false);
    }
}
