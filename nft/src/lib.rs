#![no_std]
use gstd::{msg, prelude::*, ActorId};
use nft_io::*;

const ZERO_ID: ActorId = ActorId::new([0u8; 32]);

#[derive(Debug, Default, Clone)]
pub struct NonFungibleToken {
    pub name: String,
    pub symbol: String,
    pub owner_by_id: BTreeMap<TokenId, ActorId>,
    pub token_approvals: BTreeMap<TokenId, Vec<ActorId>>,
    pub token_metadata_by_id: BTreeMap<TokenId, Option<TokenMetadata>>,
    pub tokens_for_owner: BTreeMap<ActorId, Vec<TokenId>>,
}

static mut NON_FUNGIBLE_TOKEN: Option<NonFungibleToken> = None;

impl NonFungibleToken {

    fn get_owner(&self, token_id: TokenId) -> ActorId{
        *self
            .owner_by_id
            .get(&token_id)
            .expect("NonFungibleToken: token does not exist")
    }

    fn assert_zero_address(&self, account: &ActorId) {
        if account == &ZERO_ID {
            panic!("NonFungibleToken: Zero address");
        }
    }

    fn assert_owner(&self, owner: &ActorId) {
        if !(owner == &msg::source()) { 
            panic!("Account is not an owner ");
        }
    }

    fn assert_can_transfer(&self, token_id: TokenId, owner: &ActorId) {
        if let Some(approved_accounts) = self.token_approvals.get(&token_id) {
            if approved_accounts.contains(&msg::source()) {
                return;
            }
        }
        self.assert_owner(owner);
    }

    fn mint(&mut self, to: &ActorId, token_id: TokenId, token_metadata: Option<TokenMetadata>) {
        
        if self.owner_by_id.insert(token_id, *to).is_some() { // проверка на существование токена
            panic!("Token already exists");                              // если не создан, создает
        }

        self.assert_zero_address(to); // проверка на нулевой адресс
        self.tokens_for_owner // пополняем BTreeMap<ActorId, Vec<TokenId>>
            .entry(*to)     // если еще нету такого такого пользователя, то создаем
            .and_modify(|tokens| tokens.push(token_id)) // если есть, то просто пополянем массив токенов
            .or_insert_with(|| vec![token_id]);
        
        self.token_metadata_by_id.insert(token_id, token_metadata); // пополняем мета данные 
        msg::reply(
            NFTEvent::Transfer {
                from: ZERO_ID,
                to: *to,
                token_id,
            },
            0,
        )
        .expect("Error in reply [NFTEvent::Transfer] in mint function");
    }

    

    fn transfer(&mut self, to: &ActorId, token_id: TokenId) {
        let owner = self.get_owner(token_id);

        self.assert_can_transfer(token_id, &owner);
        self.assert_zero_address(to);
        self.owner_by_id.insert(token_id, *to);

        self.tokens_for_owner
        .entry(*to)
        .and_modify(|tokens| tokens.push(token_id))
        .or_insert_with(|| vec![token_id]); // добавляем token_id новому владельцу

        self.tokens_for_owner
        .entry(owner)
        .and_modify(|tokens| tokens.retain(|&token| token != token_id)); //удаляем token_id у предыдущего владельца(оставляем все кроме одного, если быть точнее)
        
        self.token_approvals.remove(&token_id);

        msg::reply(
            NFTEvent::Transfer {
                from: owner,
                to: *to,
                token_id,
            },
            0,
        )
        .expect("Error in reply [NFTEvent::Transfer] in transfer function");
        
    }


    fn approve(&mut self, to: &ActorId, token_id: TokenId) {
        let owner = self.get_owner(token_id);

        self.assert_owner(&owner);
        self.assert_zero_address(to);

        self.token_approvals
            .entry(token_id)
            .and_modify(|approvals| approvals.push(*to))
            .or_insert_with(|| vec![*to]);

        msg::reply(
            NFTEvent::Approval {
                owner,
                approved_account: *to,
                token_id,
            },
            0,
        )
        .expect("Error in reply [NFTEvent::Approval]");
    }


    fn burn(&mut self, token_id: TokenId) {

        let owner = self.get_owner(token_id);

        self.assert_owner(&owner);
        self.owner_by_id.remove(&token_id);
        self.token_metadata_by_id.remove(&token_id);
        self.tokens_for_owner
            .entry(owner)
            .and_modify(|tokens| tokens.retain(|&token| token != token_id));

        msg::reply(
            NFTEvent::Transfer {
                from: owner,
                to: ZERO_ID,
                token_id,
            },
            0,
        )
        .expect("Error in reply [NFTEvent::Transfer] in burn function");
    
    }
    
    fn revoke_approval(&mut self, denial_account: ActorId, token_id: TokenId){
        let owner = self.get_owner(token_id);
        self.assert_owner(&owner);
        self.token_approvals
            .entry(token_id)
            .and_modify(|actor_ids| actor_ids.retain(|&actor_id| actor_id != denial_account));

        msg::reply(
            NFTEvent::Cancelapproval { 
                owner: owner, 
                denial_account: denial_account, 
                token_id: token_id, },
            0,
        )
        .expect("Error in reply [NFTEvent::Сancel_approval] in revoke_approval function");
    }

    fn get_owner_by_id(&self, token_id: TokenId) {
        let owner = *self
                            .owner_by_id
                            .get(&token_id)
                            .expect("NonFungibleToken: token does not exist");
        
        msg::reply(
            NFTEvent::GetOwner { 
                owner: owner,
            },
            0,
        )
        .expect("Error in reply [NFTEvent::Сancel_approval] in revoke_approval function");
        
    }


}




#[no_mangle]
pub unsafe extern "C" fn handle() {
    let action: NFTAction = msg::load().expect("Could not load Action");
    let nft: &mut NonFungibleToken = NON_FUNGIBLE_TOKEN.get_or_insert(NonFungibleToken::default());
    match action {
        NFTAction::Mint {
            to,
            token_id,
            token_metadata,
        } => nft.mint(&to, token_id, token_metadata),
        NFTAction::Burn { token_id } => nft.burn(token_id),

        NFTAction::RevokeApproval { to, token_id } => nft.revoke_approval(to, token_id),

        NFTAction::Transfer { to, token_id } => nft.transfer(&to, token_id),

        NFTAction::Approve { to, token_id } => nft.approve(&to, token_id),

        NFTAction::GetOwner { token_id } => nft.get_owner_by_id(token_id),
    }
}




#[no_mangle]
pub unsafe extern "C" fn init() {
    let config: InitNFT = msg::load().expect("Unable to decode init message");
    let nft = NonFungibleToken {
        name: config.name,
        symbol: config.symbol,
        ..NonFungibleToken::default()
    };
    NON_FUNGIBLE_TOKEN = Some(nft);
}


#[no_mangle]

pub unsafe extern "C" fn meta_state() -> *mut [i32; 2] {

    let state = msg::load().expect("failed to decode NFTMetaState");
    let nft: &mut NonFungibleToken = NON_FUNGIBLE_TOKEN.get_or_insert(NonFungibleToken::default());

    let encoded = match state {
        NFTMetaState::NFTInfo => NFTMetaStateReply::NFTInfo{
            name: nft.name.clone(),
            symbol: nft.symbol.clone(),
        }.encode(),

        NFTMetaState::TokensForOwner{token} => NFTMetaStateReply::TokensForOwner{
            token_approvals: nft.token_approvals[&token].clone(),
            token_metadata_by_id: nft.token_metadata_by_id[&token].clone(),
        }.encode(),

        NFTMetaState::TotalSupply => {
            NFTMetaStateReply::TotalSupply(nft.owner_by_id.len().try_into().unwrap()).encode()
        }

        NFTMetaState::SupplyForOwner{owner} => {
            NFTMetaStateReply::SupplyForOwner(nft.tokens_for_owner[&owner].len().try_into().unwrap()).encode()
        }

        
        NFTMetaState::AllTokens => NFTMetaStateReply::AllTokens {
            owner_by_id: nft.owner_by_id.clone(),
            token_approvals: nft.token_approvals.clone(),
            token_metadata_by_id: nft.token_metadata_by_id.clone(),
        }
        .encode(),
    };
    gstd::util::to_leak_ptr(encoded)
}

#[cfg(test)]
extern crate std;
#[cfg(test)]
// use std::println;

#[cfg(test)]
mod tests {
    use crate::*;
    use gtest::{Program, RunResult, System};

    // pub const NAME: String = String::from("Timurium");
    // pub const SYMBOL: String = String::from("Eagle");

    pub const USER_1: u64 = 2;
    pub const USER_2: u64 = 3;
    pub const USER_3: u64 = 4;
    pub const TOKEN_ID_1: u64 = 1;
    pub const TOKEN_ID_2: u64 = 2;
    pub const TOKEN_METADATA_1: Option<TokenMetadata> = None;

    fn init_nft(sys: &System) {
        sys.init_logger();
        let nft = Program::current(&sys);
        let res = nft.send(
            USER_1,
            InitNFT {
                name: String::from("Timurium"),
                symbol: String::from("Eagle"),
            },
        );
        assert!(res.log().is_empty());
    }



    fn mint(nft: &Program, from: u64,
        to: ActorId,
        token_id: TokenId,
        token_metadata: Option<TokenMetadata> ) -> RunResult {

        nft.send(from, NFTAction::Mint { to: to, token_id: token_id, token_metadata: token_metadata })
    }

    fn transfer(nft: &Program, from: u64,
        to: ActorId,
        token_id: TokenId) -> RunResult {

        nft.send(from, NFTAction::Transfer { to: to, token_id: token_id })
    }

    fn approve(nft: &Program, from: u64,
        to: ActorId,
        token_id: TokenId) -> RunResult {

        nft.send(from, NFTAction::Approve  { to: to, token_id: token_id })
    }

    fn burn(nft: &Program, from: u64,
        token_id: TokenId) -> RunResult {

        nft.send(from, NFTAction::Burn { token_id:token_id } )
    }

    fn revoke_approval(nft: &Program, from: u64,
        to: ActorId,
        token_id: TokenId) -> RunResult {

        nft.send(from, NFTAction::RevokeApproval { to: to, token_id: token_id })
    }

    #[test]
    fn mint_success() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        let res = mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);

        assert!(res.contains(&(
            USER_1,
            NFTEvent::Transfer {
                from: ZERO_ID,
                to: USER_1.into(),
                token_id: TOKEN_ID_1.into(),
            }.encode()
        )));

        let state: NFTMetaStateReply = nft.meta_state(NFTMetaState::NFTInfo).expect("Error in meta_state: NFTInfo ");
        let number_of_tokens: NFTMetaStateReply = nft.meta_state(NFTMetaState::TotalSupply).expect("Error in meta_state: TotalSupply");

        match state{
            NFTMetaStateReply::NFTInfo { name, symbol } => assert_eq!((name,symbol),(String::from("Timurium"), String::from("Eagle"))),
            _ => (),
        }

        match number_of_tokens{
            NFTMetaStateReply::TotalSupply(number_of_tokens) => assert_eq!(number_of_tokens,1),
            _ => (),
        }
        
    }


    #[test]
    fn transfer_success() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        let res = transfer(&nft, USER_1, USER_2.into(), TOKEN_ID_1.into());
        assert!(res.contains(&(
            USER_1,
            NFTEvent::Transfer {
                from: USER_1.into(),
                to: USER_2.into(),
                token_id: TOKEN_ID_1.into(),
            }.encode()
        )));

        let state_1: NFTMetaStateReply = nft.meta_state(NFTMetaState::SupplyForOwner { owner: USER_1.into() }).expect("Error in meta_state: SupplyForOwner");
        let state_2: NFTMetaStateReply = nft.meta_state(NFTMetaState::SupplyForOwner { owner: USER_2.into() }).expect("Error in meta_state: SupplyForOwner");

        match state_1{
            NFTMetaStateReply::SupplyForOwner(number_of_tokens) => assert_eq!(number_of_tokens,0),
            _ => (),
        }

        match state_2{
            NFTMetaStateReply::SupplyForOwner(number_of_tokens) => assert_eq!(number_of_tokens,1),
            _ => (),
        }
    }

    #[test]
    fn burn_success() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        let res = burn(&nft, USER_1, TOKEN_ID_1.into());
        assert!(res.contains(&(
            USER_1,
            NFTEvent::Transfer {
                from: USER_1.into(),
                to: ZERO_ID,
                token_id: TOKEN_ID_1.into(),
            }.encode()
        )));

        let state_1: NFTMetaStateReply = nft.meta_state(NFTMetaState::SupplyForOwner { owner: USER_1.into() }).expect("Error in meta_state: SupplyForOwner");
        match state_1{
            NFTMetaStateReply::SupplyForOwner(number_of_tokens) => assert_eq!(number_of_tokens,0),
            _ => (),
        }
    }

    #[test]
    fn approve_success() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        let res = approve(&nft, USER_1, USER_2.into(), TOKEN_ID_1.into());
        assert!(res.contains(&(
            USER_1,
            NFTEvent::Approval {
                owner: USER_1.into(),
                approved_account: USER_2.into(),
                token_id: TOKEN_ID_1.into(),
            }.encode()
        )));

        let state: NFTMetaStateReply = nft.meta_state(NFTMetaState::TokensForOwner { token: TOKEN_ID_1.into() } ).expect("Error in meta_state: TokensForOwner");

        match state{
            NFTMetaStateReply::TokensForOwner { token_approvals, token_metadata_by_id } => {
                assert_eq!(token_approvals[0], USER_2.into());
                assert!(token_metadata_by_id.is_none())
            }
            _ => (),
        }
    }

    #[test]
    fn revoke_approval_success() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        approve(&nft, USER_1, USER_2.into(), TOKEN_ID_1.into());
        approve(&nft, USER_1, USER_3.into(), TOKEN_ID_1.into());

        let res = revoke_approval(&nft, USER_1, USER_2.into(), TOKEN_ID_1.into());
        assert!(res.contains(&(
            USER_1,
            NFTEvent::Cancelapproval {
                owner: USER_1.into(),
                denial_account: USER_2.into(),
                token_id: TOKEN_ID_1.into(),
            }.encode()
        )));

        let state: NFTMetaStateReply = nft.meta_state(NFTMetaState::TokensForOwner { token: TOKEN_ID_1.into() } ).expect("Error in meta_state: TokensForOwner");

        match state{
            NFTMetaStateReply::TokensForOwner { token_approvals, token_metadata_by_id } => {
                assert_eq!(token_approvals[0], USER_3.into());
                assert!(token_metadata_by_id.is_none())
            }
            _ => (),
        }
    }

    #[test]
    fn mint_failures() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        // should fall since such a token has already been created
        assert!(mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1).main_failed());
        // should fall because there will be an attempt to create a token to a zero addres
        assert!(mint(&nft, USER_1, ZERO_ID, TOKEN_ID_2.into(), TOKEN_METADATA_1).main_failed());
    }

    #[test]
    fn transfer_failures() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        // should fall because the user does not have access to the token
        assert!(transfer(&nft, USER_2, USER_1.into(), TOKEN_ID_1.into()).main_failed());
        // should fall because the user tried to skip to a null address
        assert!(transfer(&nft, USER_1, ZERO_ID, TOKEN_ID_1.into()).main_failed());
    }

    #[test]
    fn approve_failures() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        // should fall because the user does not have access to the token
        assert!(approve(&nft, USER_2, USER_1.into(), TOKEN_ID_1.into()).main_failed());
        // should fall because the user tried to skip to a null address
        assert!(approve(&nft, USER_1, ZERO_ID, TOKEN_ID_1.into()).main_failed());
    }

    #[test]
    fn burn_failures() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        // should fall because the user does not have access to the token
        assert!(burn(&nft, USER_2, TOKEN_ID_1.into()).main_failed());
    }

    #[test]
    fn revoke_approval_failures() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);
        // should fall because the user does not have access to the token
        assert!(revoke_approval(&nft, USER_2, USER_1.into(), TOKEN_ID_1.into()).main_failed());
    }

    #[test]
    fn get_owner() {
        let sys = System::new();
        init_nft(&sys);
        let nft = sys.get_program(1);

        mint(&nft, USER_1, USER_1.into(), TOKEN_ID_1.into(), TOKEN_METADATA_1);

        let res = nft.send(USER_1, NFTAction::GetOwner { token_id: TOKEN_ID_1.into() } );
        
        assert!(res.contains(&(
            USER_1,
            NFTEvent::GetOwner { owner: USER_1.into() } 
            .encode()
        )));


        
    }

}