#![no_std]
use codec::{Decode, Encode};
use gstd::{msg, prelude::*, ActorId};
use scale_info::TypeInfo;


#[derive(Encode, Decode)]
pub struct InitEscrow {
    price: u128,
    seller: ActorId,
    buyer: ActorId,
}


#[derive(PartialEq, Debug, Encode, Decode, TypeInfo, Clone)]
 pub enum EscrowState {
    AwaitingPayment,
    AwaitingDelivery,
    Complete,
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum EscrowAction {
    Deposit,
    Delivery,
}

#[derive(Debug, Decode, Encode, TypeInfo)]
pub enum EscrowEvent {
    Deposited,
    Delivered,
}

impl Default for EscrowState {
    fn default() -> Self {
        Self::AwaitingPayment
    }
}


#[derive(Default)]
pub struct Escrow {
    state: EscrowState,
    price: u128,
    seller: ActorId,
    buyer: ActorId,
}

impl Escrow {
    fn deposit(&mut self) {
        self.check_state(EscrowState::AwaitingPayment);
        self.check_msg_source(self.buyer);
        self.check_price(self.price);
        self.state = EscrowState::AwaitingDelivery;
        msg::reply(EscrowEvent::Deposited, 0).expect("Error in reply `EscrowEvent::Deposited`");
    }

    fn delivery(&mut self) {
        self.check_state(EscrowState::AwaitingDelivery);
        self.check_msg_source(self.buyer);
        self.state = EscrowState::Complete;
        msg::send(self.seller, b"Escrow is Complete", self.price)
            .expect("Error in sending value to borrower");
        msg::reply(EscrowEvent::Delivered, 0).expect("Error in reply `EscrowEvent::Delivered`");

    }

    fn check_state(&self, state: EscrowState) {
        if self.state != state {
            panic!("Escrow must be in the {:?} state, now is {:?}", state, self.state)
        }
    }

    fn check_msg_source(&self, seller_id: ActorId ) {
        if msg::source() != seller_id {
            panic!("`msg::source` must be {:?} seller_id", seller_id)
        }
    }

    fn check_price(&self, price: u128) {
        if msg::value() != price {
            panic!("Must attached the {:?} price", price)
        }
    }
}

    static mut ESCROW: Option<Escrow> = None;



#[no_mangle]
pub unsafe extern "C" fn init() {
    let escrow_config: InitEscrow = msg::load().expect("Unable to decode InitEscrow");
    let escrow = Escrow {
        seller: escrow_config.seller,
        buyer: escrow_config.buyer,
        price: escrow_config.price,
        ..Escrow::default()
    };
    ESCROW = Some(escrow);
}

#[no_mangle]
pub unsafe extern "C" fn handle() {
    let action: EscrowAction = msg::load().expect("Could not load EscrowAction");
    let escrow: &mut Escrow = ESCROW.get_or_insert(Escrow::default()); //??????????
    match action {
        EscrowAction::Deposit => escrow.deposit(),
        EscrowAction::Delivery => escrow.delivery(),
    }
}

#[cfg(test)]
extern crate std;
#[cfg(test)]
use std::println;

#[cfg(test)]
mod tests {
    use crate::*;
    use gtest::{Program, RunResult, System};

    pub const SELLER: u64 = 2;
    pub const BUYER: u64 = 3;
    pub const PRICE: u128 = 1000;

    fn init_escrow(sys: &System) {
        sys.init_logger();
        let escrow = Program::current(&sys);
        let res = escrow.send(
            SELLER,
            InitEscrow {
                seller: SELLER.into(),
                buyer: BUYER.into(),
                price: PRICE,
            },
        );
        assert!(res.log().is_empty());
    }

    fn deposit(escrow: &Program, from: u64, amount: u128) -> RunResult {
        escrow.send_with_value(from, EscrowAction::Deposit, amount)
    }
    fn delivery(escrow: &Program, from: u64) -> RunResult {
        escrow.send(from, EscrowAction::Delivery)
    }


    #[test]
    fn deposit_success() {
        let sys = System::new();
        init_escrow(&sys);
        let escrow = sys.get_program(1);
        sys.mint_to(BUYER, 1000);
        let res = deposit(&escrow, BUYER, 1000);
        assert!(res.contains(&(BUYER, EscrowEvent::Deposited.encode())));
    }

    #[test]
    fn delivery_success() {
        let sys = System::new();
        init_escrow(&sys);
        let escrow = sys.get_program(1);
        sys.mint_to(BUYER, 1000);
        println!("BUYER BALANCE: {}", sys.balance_of(BUYER));
        deposit(&escrow, BUYER, 1000);
        let res = delivery(&escrow, BUYER);
        sys.claim_value_from_mailbox(SELLER);
        println!("BUYER BALANCE: {}", sys.balance_of(BUYER));
        println!("SELLER BALANCE: {}", sys.balance_of(SELLER));
        assert!(res.contains(&(BUYER, EscrowEvent::Delivered.encode())));
    }

    #[test]
    fn deposit_failures() {
        let sys = System::new();
        init_escrow(&sys);
        let escrow = sys.get_program(1);
        sys.mint_to(SELLER, 1000);
        sys.mint_to(BUYER, 1000);
        // must fail since the caller account is not a BUYER
        assert!(deposit(&escrow, SELLER, 1000).main_failed());
        // must fail since attached value is not equal to the amount indicated in the contract
        assert!(deposit(&escrow, BUYER, 900).main_failed());
        // check state
        sys.mint_to(BUYER, 2000);
        deposit(&escrow, BUYER, 1000);
        delivery(&escrow, BUYER);
        assert!(deposit(&escrow, BUYER, 1000).main_failed());
    }
    #[test]
    fn delivery_failures() {
        let sys = System::new();
        init_escrow(&sys);
        let escrow = sys.get_program(1);
        sys.mint_to(SELLER, 1000);
        sys.mint_to(BUYER, 1000);
        // must fail because the state is different
        assert!(delivery(&escrow, BUYER).main_failed());
        // must fail since the caller account is not a BUYER
        assert!(delivery(&escrow, SELLER).main_failed());
        
    }

}

    

