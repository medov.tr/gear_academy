use codec::Encode;
use auction_io::*;
mod utils; 
pub use utils::*;
use gtest::{Program, System};
use ft_io::*;


fn init_auction(sys: &System){
    let auction = Program::current(sys);
    let res = auction.send(USERS[0], AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });
    assert!(res.log().is_empty());
}

#[test]
fn start_auction_success(){
    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    init_ft(&sys);
    let auction = sys.get_program(1);
    let res = auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into() ,nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });

    assert!(res.contains(&(
        SELLER,
        AuctionEvent::AuctionStarted {
            auction_id: 1.into(),
            nft_contract_id: 2.into(),
            ft_contract_id: 3.into(),
            token_id: 3.into(),
            min_price: MIN_PRICE.into(),
            // получить в данном случае ended_at можно только с помощью meta_state ?
            // ended_at: self.ended_at
        }.encode()
    )));
}


#[test]
fn make_bid_success(){
    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    init_ft(&sys);

    let auction = sys.get_program(1);
    let ft = sys.get_program(3);
    auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });

    ft.send(USERS[1], FTAction::Mint {to: USERS[1].into(), amount: 2000.into()});
    ft.send(USERS[1], FTAction::Approve { to: 1.into(), amount: 2000.into() });
    let res = auction.send(USERS[1], AuctionAction::MakeBid {bid: 2000});

    
    assert!(res.contains(&(
        USERS[1],
        AuctionEvent::BidMade {
            bid: 2000,
        }.encode()
    )));

    let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: 1.into() } ).expect("Error in meta_state: Balance");
    match balance{
        FTMetaStateReply::Balance { amount } => assert_eq!(amount, 2000.into()),
        _ => (),
    }

    ft.send(USERS[2], FTAction::Mint {to: USERS[2].into(), amount: 2500.into()});
    ft.send(USERS[2], FTAction::Approve { to: 1.into(), amount: 2500.into() });
    let res = auction.send(USERS[2], AuctionAction::MakeBid {bid: 2500});


    assert!(res.contains(&(
        USERS[2],
        AuctionEvent::BidMade {
            bid: 2500 as u128,
        }.encode()
    )));

    let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USERS[1].into() } ).expect("Error in meta_state: Balance");
    match balance{
        FTMetaStateReply::Balance { amount } => assert_eq!(amount, 2000.into()),
        _ => (),
    }
    let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: USERS[2].into() } ).expect("Error in meta_state: Balance");
    match balance{
        FTMetaStateReply::Balance { amount } => assert_eq!(amount, 0.into()),
        _ => (),
    }


}

#[test]
fn end_auction_success(){
    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    init_ft(&sys);

    let auction = sys.get_program(1);
    let ft = sys.get_program(3);
    auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });
    let bid: u128 = 2000;
    ft.send(USERS[1], FTAction::Mint {to: USERS[1].into(), amount: 2000.into()});
    ft.send(USERS[1], FTAction::Approve { to: 1.into(), amount: 2000.into() });
    auction.send(USERS[1], AuctionAction::MakeBid {bid: 2000});
    sys.spend_blocks(DURATION as u32);
    let res = auction.send(SELLER, AuctionAction::EndAuction);

    
    assert!(res.contains(&(
        SELLER,
        AuctionEvent::AuctionEnded {
            winner: USERS[1].into(),
            highest_bid: bid,
        }.encode()
    )));

    let balance: FTMetaStateReply = ft.meta_state(FTMetaState::Balance { owner: SELLER.into() } ).expect("Error in meta_state: Balance");
    match balance{
        FTMetaStateReply::Balance { amount } => assert_eq!(amount, 2000.into()),
        _ => (),
    }


}



#[test]
fn start_auction_failures(){

    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    let auction = sys.get_program(1);
    // The auction is not started by the owner of the token
    assert!(auction.send(USERS[0], AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() }).main_failed());
    // Auction duration must be equal or more than bid period
    assert!(auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: (DURATION/10000).into(), min_price: MIN_PRICE.into() }).main_failed());
    // Price must be more than 500
    assert!(auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: (MIN_PRICE/10).into() }).main_failed());
    // Auction already exists
    auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });
    assert!(auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() }).main_failed());
}


#[test]
fn make_bid_failures(){


    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    init_ft(&sys);

    let auction = sys.get_program(1);
    let ft = sys.get_program(3);

    // Auction must exists
    assert!(auction.send(USERS[1], AuctionAction::MakeBid {bid: 2000}).main_failed());

    // bid must be more than the current bid
    auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });
    ft.send(USERS[1], FTAction::Mint {to: USERS[1].into(), amount: 2000.into()});
    ft.send(USERS[1], FTAction::Approve { to: 1.into(), amount: 2000.into() });
    assert!(auction.send(USERS[1], AuctionAction::MakeBid {bid: 999}).main_failed());

    // Auction is over
    sys.spend_blocks(DURATION as u32);
    assert!(auction.send(USERS[1], AuctionAction::MakeBid {bid: 2000}).main_failed());

}

#[test]
fn end_auction_failures(){
    let sys = System::new();
    sys.init_logger();
    init_auction(&sys);
    init_mock_nft(&sys);
    init_ft(&sys);

    let auction = sys.get_program(1);
    let _ft = sys.get_program(3);
    // Auction must exists
    assert!(auction.send(SELLER, AuctionAction::EndAuction).main_failed());
    // Auction must be over
    auction.send(SELLER, AuctionAction::StartAuction { auction_id: 1.into(), nft_contract_id: 2.into(), ft_contract_id: 3.into(), token_id: 3.into(), duration: DURATION.into(), min_price: MIN_PRICE.into() });
    assert!(auction.send(SELLER, AuctionAction::EndAuction).main_failed());
    // Only the owner can finish the auction
    sys.spend_blocks(DURATION as u32);
    assert!(auction.send(USERS[0], AuctionAction::EndAuction).main_failed());
}
